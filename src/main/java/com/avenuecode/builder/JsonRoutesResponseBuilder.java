package com.avenuecode.builder;

import java.util.List;

import com.avenuecode.model.RouteResponsePayload;
import com.avenuecode.model.RouteReturn;

public class JsonRoutesResponseBuilder {
	
	private RouteResponsePayload response;
	private List<List<Integer>> paths;

	public JsonRoutesResponseBuilder(List<List<Integer>> paths){
		this.response = new RouteResponsePayload();
		this.paths = paths;
	}
	
	public JsonRoutesResponseBuilder buildResponse(){
		
		for(List<Integer> route : this.paths){
			RouteReturn routeReturn = new RouteReturn();
			for(Integer vertex : route){
				String nextVertex = Dictionary.getLetter(vertex);
				routeReturn.appendVertexToRouteName(nextVertex);
				routeReturn.addOneStop();
			}
			this.response.addRoute(routeReturn);
		}
		return this;
	}
	
	public RouteResponsePayload getResponse(){
		return this.response;
	}
}
