package com.avenuecode.builder;

import java.util.LinkedHashSet;
import java.util.Set;

import com.avenuecode.model.DirectedEdge;
import com.avenuecode.model.EdgeWeightedDigraph;
import com.avenuecode.model.JsonDigraph;
import com.avenuecode.model.JsonRoute;


/**
 * This class is responsible for
 * taking a JsonGraph and parse it
 * to a Graph
 * @author Cassiano
 *
 */
public class GraphBuilder {
	
	private EdgeWeightedDigraph diGraph;
	private JsonDigraph jsonDigraph;
	
	public GraphBuilder(JsonDigraph jsonGraph) {
		this.jsonDigraph = jsonGraph;
	}
	
	public GraphBuilder buildGraph(){
		
		buildMyRealm();
		
		
		return this;
	}
	
	/**
	 * Towns are verticies and routes
	 * are edges.
	 */
	private void buildMyRealm(){
		this.diGraph = new EdgeWeightedDigraph(getVerticiesSize());
		for (JsonRoute jsonRoute: this.jsonDigraph.getData()) {
			DirectedEdge diEdge = 
					new DirectedEdge(Dictionary.getInteger(jsonRoute.getSource()),
									 Dictionary.getInteger(jsonRoute.getTarget()), 
									 jsonRoute.getDistance());
			this.diGraph.addEdge(diEdge);
			
			
		}
		
	}
	
	private int getVerticiesSize(){
		//using Set to avoid duplicates
		Set<String> towns = new LinkedHashSet<>();
		for (JsonRoute jsonRoute: this.jsonDigraph.getData()) {
			
			towns.add(jsonRoute.getSource());
			towns.add(jsonRoute.getTarget());
		}
		return towns.size();
	}
	
	public JsonDigraph getJsonGraph() {
		return jsonDigraph;
	}
	public void setJsonGraph(JsonDigraph jsonGraph) {
		this.jsonDigraph = jsonGraph;
	}

	public EdgeWeightedDigraph getDiGraph() {
		return this.diGraph;
	}
	
	
	public static void main(String[] args) {
		for (char ch = 'A'; ch <= 'Z'; ch++) {
		    int i = (int) ch;  // cast desnecessario, mas explicativo
		    System.out.println(ch + "  " + i);
		}
		for (int i = 65; i <= 90; i++) {
		    char ch = (char) i;
		    System.out.println(i + "  " + ch);
		}
	}
	
}
