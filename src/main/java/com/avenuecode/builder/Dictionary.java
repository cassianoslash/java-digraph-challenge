package com.avenuecode.builder;

import java.util.HashMap;
import java.util.Map;

/**
 * This class only maps the alphabet to numbers
 * in a HashMap.
 * @author Cassiano
 *
 */
public final class Dictionary {
	private static Map<String, Integer> dic = new HashMap<>();
	private static Map<Integer, String> reverseDic = new HashMap<>();
	/*this avoid this class to be instanciated*/
	private Dictionary(){}
	static {
		int count = 0;
		for (char ch = 'A'; ch <= 'Z'; ch++) {
			dic.put(String.valueOf(ch), count);
			count++;
		    
		}
		
		count = 0;
		for (char ch = 'A'; ch <= 'Z'; ch++) {
			reverseDic.put(count, String.valueOf(ch));
			count++;
		    
		}
	}
	
	public static int getInteger(String letter){
		return dic.get(letter);
	}
	
	public static String getLetter(int integer){
		return reverseDic.get(integer);
	}
	
}
