package com.avenuecode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	
		
//		Conta c = new Conta();
//		c.setName("conta bradesco");
//		
//		EntityManagerFactory emf = Persistence.createEntityManagerFactory("default");
//		EntityManager em = emf.createEntityManager();
//		
//		em.getTransaction().begin();
//		
//		em.persist(c);
//		
//		em.getTransaction().commit();
//		
//		em.close();
//		emf.close();
	}

}
