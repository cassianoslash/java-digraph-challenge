package com.avenuecode.services;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.avenuecode.builder.Dictionary;
import com.avenuecode.builder.GraphBuilder;
import com.avenuecode.builder.JsonRoutesResponseBuilder;
import com.avenuecode.cool.algorithms.DijkstraSP;
import com.avenuecode.dao.JsonDigraphDAO;
import com.avenuecode.model.EdgeWeightedDigraph;
import com.avenuecode.model.JsonDigraph;
import com.avenuecode.model.RouteResponsePayload;
import com.avenuecode.model.ShortestPathReturn;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class GraphService {
	
	/**
	 * This endpoint should receive a graph and store it in the 
	 * database for future references. It should associate an 
	 * integer identifier to the graph and send it on the 
	 * response body.
	 * @param jsonRequest
	 * @return A json on the format 
	 * { "id" : 1,
	   "data":[
      { 
        "source": "A", "target": "B", "distance":6
      },
      { 
        "source": "A", "target": "E", "distance":4
      }
      ]}

	 */
    @PostMapping(value = "/graph")
    public ResponseEntity<String> saveGraph(@RequestBody String jsonRequest) {
    	
        try{
        	ObjectMapper objectMapper = new ObjectMapper();
        	JsonDigraph jsonDigraph = objectMapper.readValue(jsonRequest, JsonDigraph.class);
        	
        	JsonDigraphDAO jsDigraphDAO = new JsonDigraphDAO();
        	jsDigraphDAO.save(jsonDigraph);
        	
        	//Object to JSON in String
        	String jsonInString = objectMapper.writeValueAsString(jsonDigraph);
        	
        	return new ResponseEntity<>(jsonInString, HttpStatus.CREATED);
        }catch (Exception ex) {
			return new ResponseEntity<>(ex.getMessage(), HttpStatus.METHOD_FAILURE);
		}
    }
	
	/**
	 * This endpoint should retrieve a previously saved graph from the database.
	 * If the graph doesn't exist, should return a NOT FOUND error response.
	 * @param id
	 * @return A json on the format 
	 * { 
	 * "id" : 1,
	   "data":[
      { 
        "source": "A", "target": "B", "distance":6
      },
      { 
        "source": "A", "target": "E", "distance":4
      }
      ]}

	 */
    @GetMapping(value = "/graph/{id}")
    public ResponseEntity<String> retrieveGraph(@PathVariable Integer id) {
    	
        try{
        	ObjectMapper objectMapper = new ObjectMapper();
        	
        	JsonDigraphDAO digraphDAO = new JsonDigraphDAO();
        	
        	//Object to JSON in String
        	String jsonInString = objectMapper.writeValueAsString(digraphDAO.retrieve(id));
        	
        	if(jsonInString.isEmpty() || "null".equalsIgnoreCase(jsonInString))
        		throw new RuntimeException("NOT_FOUND");
        	
        	return new ResponseEntity<>(jsonInString, HttpStatus.OK);
        	
        }catch (NumberFormatException nfe){
        	return new ResponseEntity<>(nfe.getMessage(), HttpStatus.METHOD_FAILURE);
        }catch (Exception ex) {
			return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
		}
    }
	
	/**
	 * This endpoint should compute all available routes from 
	 * any given pair of towns within a given maximum number
	 * 	of stops. If there's no available routes, the result 
	 * 	should be an empty list. In case the parameter "maxStops" 
	 * 	is not provided, you should list all routes for the 
	 * 	given pair of towns.

	 * 	For instance, in the graph (AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7), 
	 * 	the possible routes from A to C with maximum of 3 
		stops would be: ["ABC", "ADC", "AEBC"]
	 * @param jsonRequest
	 * @return{
				  "routes": [
				    {
				      "route": "ABC",
				      "stops": 2
				    },
				    {
				      "route": "ADC",
				      "stops": 2
				    },
				    {
				      "route": "AEBC",
				      "stops": 3
				    }
				  ]
				}
	 */
    @PostMapping(value = "/routes/from/{townFrom}/to/{townTo}")
    public ResponseEntity<String> findAvailableRoutes(@RequestBody String jsonRequest, 
    												@PathVariable String townFrom, 
    												@PathVariable String townTo, 
    												@RequestParam("maxStops") Integer maxStops) {
    	
        try{
        	ObjectMapper objectMapper = new ObjectMapper();
        	JsonDigraph jDigraph = objectMapper.readValue(jsonRequest, JsonDigraph.class);
        	
        	GraphBuilder graphBuilder = new GraphBuilder(jDigraph);
        	EdgeWeightedDigraph diGraph = graphBuilder.buildGraph().getDiGraph();
        	
        	List<List<Integer>> paths = diGraph.findAllPaths(Dictionary.getInteger(townFrom.toUpperCase()), 
        												     Dictionary.getInteger(townTo.toUpperCase()),
        												     maxStops);
        	
        	
        	JsonRoutesResponseBuilder responseBuilder = new JsonRoutesResponseBuilder(paths);
        	RouteResponsePayload routesResponse = responseBuilder.buildResponse().getResponse();
        	
        	//Object to JSON in String
        	String jsonRoutesResponse = objectMapper.writeValueAsString(routesResponse);
        	
        	return new ResponseEntity<>(jsonRoutesResponse, HttpStatus.OK);
        }catch (Exception ex) {
			return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
		}
    }
	
	/**
	 * This endpoint should do exactly the same calculation described 
	 * (in the previous section) but it should use a previously saved 
	 * graph. If the graph doesn't exist in the database, it should 
	 * return a NOT FOUND error response.
	 * @param jsonRequest
	 * @return
	 * {
		  "routes": [
		    {
		      "route": "ABC",
		      "stops": 2
		    },
		    {
		      "route": "ADC",
		      "stops": 2
		    },
		    {
		      "route": "AEBC",
		      "stops": 3
		    }
		  ]
		}

	 */
    @PostMapping(value = "/routes/{id}/from/{source}/to/{target}")
    public ResponseEntity<String> findAvailableRoutesOnSavedGraph(@PathVariable Integer id,
    															@PathVariable String source,
    															@PathVariable String target,
    															@RequestParam("maxStops") Integer maxStops) {
    	
        try{
        	ObjectMapper objectMapper = new ObjectMapper();
        	
        	JsonDigraphDAO digraphDAO = new JsonDigraphDAO();
        	
        	//Object to JSON in String
        	String jsonInString = objectMapper.writeValueAsString(digraphDAO.retrieve(id));
        	
        	return findAvailableRoutes(jsonInString, source, target, maxStops);
        	
        }catch (Exception ex) {
		
			return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
		}
    }
	
	/**
	 * This endpoint should receive a directed graph 
	 * and a ordered list of towns and retrieve the 
	 * total distance on walking through the list 
	 * of towns in the order they appear on the request.
	 *  If the list of towns is empty or has a single
	 *  element, the result should be zero. 
	 * If there's no path described by the list of towns, the result should be -1.
	 * @param jsonRequest
	 * @return
	 */
    @PostMapping(value = "/distance")
    public ResponseEntity<String> findDistanceForPath(@RequestBody String jsonRequest) {
    	
        try{
        	ObjectMapper objectMapper = new ObjectMapper();
        	JsonDigraph jGraph = objectMapper.readValue(jsonRequest, JsonDigraph.class);
        	
        	validatePath(jGraph);
        	
        	GraphBuilder graphBuilder = new GraphBuilder(jGraph);
        	EdgeWeightedDigraph diGraph = graphBuilder.buildGraph().getDiGraph();
        	
        	ShortestPathReturn shortestPathRet = new ShortestPathReturn();
        	
        	shortestPathRet.setDistance(diGraph.getTotalDistance(jGraph.getPath()));
        	
        	//Object to JSON in String
        	String jsonInString = objectMapper.writeValueAsString(shortestPathRet);
        	
        	return new ResponseEntity<>(jsonInString, HttpStatus.OK);
        	
        }catch (NullPointerException npe) {
    		
			return new ResponseEntity<>("-1", HttpStatus.OK);	
        }catch (IllegalArgumentException iae) {
    		
			return new ResponseEntity<>("0", HttpStatus.OK);
        }catch (Exception ex) {
		
			return new ResponseEntity<>(ex.getMessage(), HttpStatus.METHOD_FAILURE);
		}
    }
	
    /*
     * If path == null, throw NPE, if path size < 2 throw IllegalAE
     */
	private void validatePath(JsonDigraph jGraph) throws IllegalArgumentException, NullPointerException {
		int pathSize = jGraph.getPath().size();
		if(pathSize < 2)
			throw new IllegalArgumentException();
		
	}

	/**
	 * This endpoint should do exactly the same calculation described 
	 * (in the previous section) but it should use a previously saved 
	 * graph. If the graph doesn't exist in the database, it should 
	 * return a NOT FOUND error response.
	 * @param jsonRequest
	 * @return
	 */
    @PostMapping(value = "/distance/{id}")
    public ResponseEntity<String> findDistanceForPathSaved(@RequestBody String jsonRequest,
    														@PathVariable Integer id) {
    	
        try{
        	ObjectMapper objectMapper = new ObjectMapper();
        	JsonDigraphDAO digraphDAO = new JsonDigraphDAO();
        	
        	//Object to JSON in String
        	JsonDigraph jsDigraphFromDB = digraphDAO.retrieve(id);
        	JsonDigraph jsDigraphFromRquest = objectMapper.readValue(jsonRequest, JsonDigraph.class);
        	
        	jsDigraphFromDB.setPath(jsDigraphFromRquest.getPath());
        	
        	String jsonRequestFull = objectMapper.writeValueAsString(jsDigraphFromDB);
        	
        	return findDistanceForPath(jsonRequestFull);
        
        }catch (Exception ex) {
		
			return new ResponseEntity<>(ex.getMessage(), HttpStatus.METHOD_FAILURE);
		}
    }
	
	/**
	 * This endpoint should receive a directed graph and find the 
	 * shortest path between two towns. If the start and end town 
	 * are the same, the result should be zero. If there's no path 
	 * between these towns, it should be -1.
	 * @param jsonRequest
	 * @return{
			  "distance" : 3,
			  "path" : ["A", "B", "C"]
			}

	 */
    @PostMapping(value = "/distance/from/{townFrom}/to/{townTo}")
    public ResponseEntity<String> findShortestDistance(@RequestBody String jsonRequest,
											    		@PathVariable String townFrom, 
														@PathVariable String townTo) {
    	
        try{
        	ObjectMapper objectMapper = new ObjectMapper();
        	JsonDigraph jGraph = objectMapper.readValue(jsonRequest, JsonDigraph.class);
        	
        	GraphBuilder graphBuilder = new GraphBuilder(jGraph);
        	EdgeWeightedDigraph diGraph = graphBuilder.buildGraph().getDiGraph();
        	
        	DijkstraSP dij = new DijkstraSP(diGraph, 
        					Dictionary.getInteger(townFrom.toUpperCase()));
        	
        	ShortestPathReturn shortPathRet = dij.getShortestPath(diGraph, 
        									Dictionary.getInteger(townFrom.toUpperCase()),
        									Dictionary.getInteger(townTo.toUpperCase()));
        	//Object to JSON in String
        	String jsonShortPathRet = objectMapper.writeValueAsString(shortPathRet);
        	
        	return new ResponseEntity<>(jsonShortPathRet, HttpStatus.OK);
        }catch (Exception ex) {
			return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
		}
    }
	
	/**
	 * This endpoint should do exactly the same calculation
	 * described (in the previous section) but it should 
	 * use a previously saved graph. If the graph doesn't
	 * exist in the database, it should return a 
	 * NOT FOUND error response.
	 * @param jsonRequest
	 * @return
	 */
    @PostMapping(value = "/distance/{id}/from/{source}/to/{target}")
    public ResponseEntity<String> findShortestDistanceOnSavedGraph(@PathVariable Integer id,
    															  @PathVariable String source,
    															  @PathVariable String target) {
    	
        try{
        	ObjectMapper objectMapper = new ObjectMapper();
        	
        	JsonDigraphDAO digraphDAO = new JsonDigraphDAO();
        	
        	//Object to JSON in String
        	String jsStringSavedGraph = objectMapper.writeValueAsString(digraphDAO.retrieve(id));
        	
        	return findShortestDistance(jsStringSavedGraph, source, target);

        }catch (Exception ex) {
		
			return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
		}
    }
	
}
