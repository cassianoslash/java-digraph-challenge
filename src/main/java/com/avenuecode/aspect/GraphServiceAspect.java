package com.avenuecode.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class GraphServiceAspect {

	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Before(value = "execution(* com.avenuecode.services.GraphService.*(..))")
	public void beforeAdvice(JoinPoint joinPoint) {
		log.info("Before method:" + joinPoint.getSignature());
	}

	@After(value = "execution(* com.avenuecode.services.GraphService.*(..))")
	public void afterAdvice(JoinPoint joinPoint) {
		log.info("After method:" + joinPoint.getSignature());
	}
}
