package com.avenuecode.cool.algorithms;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import com.avenuecode.builder.Dictionary;
import com.avenuecode.model.DirectedEdge;
import com.avenuecode.model.EdgeWeightedDigraph;
import com.avenuecode.model.ShortestPathReturn;

/**
 *  The {@code DijkstraSP} class represents a data type for solving the
 *  single-source shortest paths problem in edge-weighted digraphs
 *  where the edge weights are nonnegative.
 *  <p>
 *  This implementation uses Dijkstra's algorithm with a binary heap.
 *  The constructor takes time proportional to <em>E</em> log <em>V</em>,
 *  where <em>V</em> is the number of vertices and <em>E</em> is the number of edges.
 *  Each call to {@code distTo(int)} and {@code hasPathTo(int)} takes constant time;
 *  each call to {@code pathTo(int)} takes time proportional to the number of
 *  edges in the shortest path returned.
 *  <p>
 *  For additional documentation,    
 *  see <a href="https://algs4.cs.princeton.edu/44sp">Section 4.4</a> of    
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne. 
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
public class DijkstraSP {
    private double[] distanceToShortestPath;
    private DirectedEdge[] lastEdgeToShortestPath;
    private IndexMinPQ<Double> verticiesPriorityQueue;    // priority queue of vertices

    private final String EDGE = "edge ";
    /**
     * Computes a shortest-paths tree from the source vertex {@code s} to every other
     * vertex in the edge-weighted digraph {@code G}.
     *
     * @param  g the edge-weighted digraph
     * @param  sourceVertex the source vertex
     * @throws IllegalArgumentException if an edge weight is negative
     * @throws IllegalArgumentException unless {@code 0 <= s < V}
     */
    public DijkstraSP(EdgeWeightedDigraph g, int sourceVertex) {
    	/*validation to avoid negative weights*/
        for (DirectedEdge e : g.edges()) {
            if (e.weight() < 0)
                throw new IllegalArgumentException(EDGE + e + " has negative weight");
        }

        distanceToShortestPath = new double[g.V()];
        lastEdgeToShortestPath = new DirectedEdge[g.V()];

        validateVertex(sourceVertex);

        for (int v = 0; v < g.V(); v++)
            distanceToShortestPath[v] = Double.POSITIVE_INFINITY;
        
        
        distanceToShortestPath[sourceVertex] = 0.0;

        // relax vertices in order of distance from s
        verticiesPriorityQueue = new IndexMinPQ<>(g.V());
        verticiesPriorityQueue.insert(sourceVertex, distanceToShortestPath[sourceVertex]);
        
        while (!verticiesPriorityQueue.isEmpty()) {
            int index = verticiesPriorityQueue.delMin();
            for (DirectedEdge e : g.adj(index))
                relax(e);
        }

        // check optimality conditions
        assert check(g, sourceVertex);
    }

    // relax edge e and update pq if changed
    private void relax(DirectedEdge edge) {
        int sourceVertex = edge.from();
        int targetVertex = edge.to();
        
        if (distanceToShortestPath[targetVertex] > distanceToShortestPath[sourceVertex] + edge.weight()) {
            distanceToShortestPath[targetVertex] = distanceToShortestPath[sourceVertex] + edge.weight();
            lastEdgeToShortestPath[targetVertex] = edge;
            
            if (verticiesPriorityQueue.contains(targetVertex)){ 
            	verticiesPriorityQueue.decreaseKey(targetVertex, distanceToShortestPath[targetVertex]);
            }
            else{
            	verticiesPriorityQueue.insert(targetVertex, distanceToShortestPath[targetVertex]);
            }
        }
    }

    /**
     * Returns the length of a shortest path from the source vertex {@code s} to vertex {@code v}.
     * @param  v the destination vertex
     * @return the length of a shortest path from the source vertex {@code s} to vertex {@code v};
     *         {@code Double.POSITIVE_INFINITY} if no such path
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public double distTo(int v) {
        validateVertex(v);
        return distanceToShortestPath[v];
    }

    /**
     * Returns true if there is a path from the source vertex {@code s} to vertex {@code v}.
     *
     * @param  v the destination vertex
     * @return {@code true} if there is a path from the source vertex
     *         {@code s} to vertex {@code v}; {@code false} otherwise
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public boolean hasPathTo(int v) {
        validateVertex(v);
        return distanceToShortestPath[v] < Double.POSITIVE_INFINITY;
    }

    /**
     * Returns a shortest path from the source vertex {@code s} to vertex {@code v}.
     *
     * @param  v the destination vertex
     * @return a shortest path from the source vertex {@code s} to vertex {@code v}
     *         as an iterable of edges, and {@code null} if no such path
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public Iterable<DirectedEdge> pathTo(int v) {
        validateVertex(v);
        if (!hasPathTo(v)) return null;
        List<DirectedEdge> path = new ArrayList<>(); 
        for (DirectedEdge e = lastEdgeToShortestPath[v]; e != null; e = lastEdgeToShortestPath[e.from()]) {
            path.add(e);
        }
        List<DirectedEdge> reversePath = new ArrayList<>();
        for(int i = path.size() -1; i >= 0; i--){
        	reversePath.add(path.get(i));
        }
        return reversePath;
    }


    // check optimality conditions:
    // (i) for all edges e:            distTo[e.to()] <= distTo[e.from()] + e.weight()
    // (ii) for all edge e on the SPT: distTo[e.to()] == distTo[e.from()] + e.weight()
    private boolean check(EdgeWeightedDigraph G, int s) {

        // check that edge weights are nonnegative
        for (DirectedEdge e : G.edges()) {
            if (e.weight() < 0) {
                System.err.println("negative edge weight detected");
                return false;
            }
        }

        // check that distTo[v] and edgeTo[v] are consistent
        if (distanceToShortestPath[s] != 0.0 || lastEdgeToShortestPath[s] != null) {
            System.err.println("distTo[s] and edgeTo[s] inconsistent");
            return false;
        }
        for (int v = 0; v < G.V(); v++) {
            if (v == s) continue;
            if (lastEdgeToShortestPath[v] == null && distanceToShortestPath[v] != Double.POSITIVE_INFINITY) {
                System.err.println("distTo[] and edgeTo[] inconsistent");
                return false;
            }
        }

        // check that all edges e = v->w satisfy distTo[w] <= distTo[v] + e.weight()
        for (int v = 0; v < G.V(); v++) {
            for (DirectedEdge e : G.adj(v)) {
                int w = e.to();
                if (distanceToShortestPath[v] + e.weight() < distanceToShortestPath[w]) {
                    System.err.println(EDGE + e + " not relaxed");
                    return false;
                }
            }
        }

        // check that all edges e = v->w on SPT satisfy distTo[w] == distTo[v] + e.weight()
        for (int w = 0; w < G.V(); w++) {
            if (lastEdgeToShortestPath[w] == null) continue;
            DirectedEdge e = lastEdgeToShortestPath[w];
            int v = e.from();
            if (w != e.to()) return false;
            if (distanceToShortestPath[v] + e.weight() != distanceToShortestPath[w]) {
                System.err.println(EDGE + e + " on shortest path not tight");
                return false;
            }
        }
        return true;
    }

    // throw an IllegalArgumentException unless {@code 0 <= v < V}
    private void validateVertex(int v) {
        int V = distanceToShortestPath.length;
        if (v < 0 || v >= V)
            throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
    }
    
    
    public ShortestPathReturn getShortestPath(EdgeWeightedDigraph g, int source, int target) {
    	ShortestPathReturn shortestPathRet = new ShortestPathReturn();
    	List<String> path = new ArrayList<>();
        if (this.hasPathTo(target)) {
        	shortestPathRet.setDistance((int) this.distTo(target));
        	path.add(Dictionary.getLetter(source));
            for (DirectedEdge e : this.pathTo(target)) {
                System.out.print(Dictionary.getLetter(e.to()) + ", ");
                path.add(Dictionary.getLetter(e.to()));
            }
            
           shortestPathRet.setPath(path);
        }
        else {
            System.out.println("no path");
        }
    	return shortestPathRet;
    }
}