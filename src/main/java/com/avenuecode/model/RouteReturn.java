package com.avenuecode.model;

public class RouteReturn {
	private String route;
	private Integer stops;
	
	
	public String getRoute() {
		return route;
	}
	public void setRoute(String route) {
		this.route = route;
	}
	public Integer getStops() {
		return stops;
	}
	public void setStops(Integer stops) {
		this.stops = stops;
	}
	
	public void appendVertexToRouteName(String nextVertex){
		if(null == this.route){
			this.route = "";
		}
		this.route += nextVertex;
	}
	
	public void addOneStop(){
		if(null == this.stops){
			/*-1 because the first vertex is not a stop*/
			this.stops = -1;
		}
		this.stops++;
	}
	

}
