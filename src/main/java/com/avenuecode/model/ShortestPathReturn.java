package com.avenuecode.model;

import java.util.List;

public class ShortestPathReturn {
	private Integer distance;
	private List<String> path;
	public Integer getDistance() {
		return distance;
	}
	public void setDistance(Integer distance) {
		this.distance = distance;
	}
	public List<String> getPath() {
		return path;
	}
	public void setPath(List<String> path) {
		this.path = path;
	}
	
	
}
