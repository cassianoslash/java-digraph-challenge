package com.avenuecode.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.stereotype.Component;

/**
 * This class represents a graph
 * in json format, provided by AC
 * @author Cassiano
 */
@Entity
@Table(name="json_digraph")
public class JsonDigraph {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@OneToMany
	private List<JsonRoute> data;
	
	@Transient
	private List<String> path;

	//Only here 'cause Hibernate asks to :(
	public JsonDigraph(){}

	public List<JsonRoute> getData() {
		return data;
	}

	public void setData(List<JsonRoute> data) {
		this.data = data;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<String> getPath() {
		return path;
	}

	public void setPath(List<String> path) {
		this.path = path;
	}
}
