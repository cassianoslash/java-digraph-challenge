package com.avenuecode.model;

import java.util.ArrayList;
import java.util.List;

import com.avenuecode.builder.Dictionary;
import com.avenuecode.cool.algorithms.Bag;
import com.avenuecode.cool.algorithms.DijkstraSP;

/**
 *  The {@code EdgeWeightedDigraph} class represents a edge-weighted
 *  digraph of vertices named 0 through <em>V</em> - 1, where each
 *  directed edge is of type {@link DirectedEdge} and has a real-valued weight.
 *  It supports the following two primary operations: add a directed edge
 *  to the digraph and iterate over all of edges incident from a given vertex.
 *  It also provides
 *  methods for returning the number of vertices <em>V</em> and the number
 *  of edges <em>E</em>. Parallel edges and self-loops are permitted.
 *  <p>
 *  This implementation uses an adjacency-lists representation, which 
 *  is a vertex-indexed array of {@link Bag} objects.
 *  All operations take constant time (in the worst case) except
 *  iterating over the edges incident from a given vertex, which takes
 *  time proportional to the number of such edges.
 *  <p>
 *  For additional documentation,
 *  see <a href="https://algs4.cs.princeton.edu/44sp">Section 4.4</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
public class EdgeWeightedDigraph {
    private static final String NEWLINE = System.getProperty("line.separator");

    private final int verticiesSize;                // number of vertices in this digraph
    private int edgesSize;                      // number of edges in this digraph
    private Bag<DirectedEdge>[] adjList;    // adj[v] = adjacency list for vertex v
    private int[] indegree;             // indegree[v] = indegree of vertex v
    
    private List<List<Integer>> allPaths;
    /**
     * Initializes an empty edge-weighted digraph with {@code V} vertices and 0 edges.
     *
     * @param  V the number of vertices
     * @throws IllegalArgumentException if {@code V < 0}
     */
    public EdgeWeightedDigraph(int V) {
        if (V < 0) throw new IllegalArgumentException("Number of vertices in a Digraph must be nonnegative");
        this.verticiesSize = V;
        this.edgesSize = 0;
        this.indegree = new int[V];
        adjList = (Bag<DirectedEdge>[]) new Bag[V];
        for (int v = 0; v < V; v++)
            adjList[v] = new Bag<>();
    }


    /**
     * Returns the number of vertices in this edge-weighted digraph.
     *
     * @return the number of vertices in this edge-weighted digraph
     */
    public int V() {
        return verticiesSize;
    }

    /**
     * Returns the number of edges in this edge-weighted digraph.
     *
     * @return the number of edges in this edge-weighted digraph
     */
    public int E() {
        return edgesSize;
    }

    // throw an IllegalArgumentException unless {@code 0 <= v < V}
    private void validateVertex(int v) {
        if (v < 0 || v >= verticiesSize)
            throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (verticiesSize-1));
    }

    /**
     * Adds the directed edge {@code e} to this edge-weighted digraph.
     *
     * @param  e the edge
     * @throws IllegalArgumentException unless endpoints of edge are between {@code 0}
     *         and {@code V-1}
     */
    public void addEdge(DirectedEdge e) {
        int v = e.from();
        int w = e.to();
        validateVertex(v);
        validateVertex(w);
        adjList[v].add(e);
        indegree[w]++;
        edgesSize++;
    }


    /**
     * Returns the directed edges incident from vertex {@code v}.
     *
     * @param  v the vertex
     * @return the directed edges incident from vertex {@code v} as an Iterable
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public Iterable<DirectedEdge> adj(int v) {
        validateVertex(v);
        return adjList[v];
    }

    /**
     * Returns the number of directed edges incident from vertex {@code v}.
     * This is known as the <em>outdegree</em> of vertex {@code v}.
     *
     * @param  v the vertex
     * @return the outdegree of vertex {@code v}
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public int outdegree(int v) {
        validateVertex(v);
        return adjList[v].size();
    }

    /**
     * Returns the number of directed edges incident to vertex {@code v}.
     * This is known as the <em>indegree</em> of vertex {@code v}.
     *
     * @param  v the vertex
     * @return the indegree of vertex {@code v}
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public int indegree(int v) {
        validateVertex(v);
        return indegree[v];
    }

    /**
     * Returns all directed edges in this edge-weighted digraph.
     * To iterate over the edges in this edge-weighted digraph, use foreach notation:
     * {@code for (DirectedEdge e : G.edges())}.
     *
     * @return all edges in this edge-weighted digraph, as an iterable
     */
    public Iterable<DirectedEdge> edges() {
        Bag<DirectedEdge> list = new Bag<>();
        for (int v = 0; v < verticiesSize; v++) {
            for (DirectedEdge e : adj(v)) {
                list.add(e);
            }
        }
        return list;
    } 
    
    /* find all paths from source to target*/
    public  List<List<Integer>> findAllPaths(int source, int target, Integer maxStops) 
    {
        boolean[] isVisited = new boolean[this.verticiesSize];
        List<Integer> pathList = new ArrayList<>();
        this.allPaths = new ArrayList<>(); 
        //add source to path[]
        pathList.add(source);
         
        //Call recursive utility
        findPathsRecursively(source, target, isVisited, pathList, maxStops);
        
        return allPaths;
    }
    
    /*
     * This is a DFS algorithm to find all
     * the possible paths.
     * When it find a path, keep it in a
     * List<Integer>.
     * PS: uses visitation to avoid loops...
     */
    private void findPathsRecursively(Integer source, Integer target,
                                    boolean[] isVisited,
                            List<Integer> localPathList,
                            Integer maxStops) {
         
        // Mark the current node
        isVisited[source] = true;
         
        if (source.equals(target)) 
        {
        	//if this path has more than @maxStops, do not add it.
        	//else @maxStops == null, it shortcuts (&&) and add all!
        	if(null != maxStops && localPathList.size() < (maxStops + 2))
        	{
        		this.allPaths.add(new ArrayList<Integer>(localPathList));
        	}
        	else if (null == maxStops)
        	{
        		this.allPaths.add(new ArrayList<Integer>(localPathList));
        	}
        }
         
        // Recur for all the vertices
        // adjacent to current vertex
        for (DirectedEdge dirEdge : adjList[source]) 
        {
            if (!isVisited[dirEdge.to()])
            {
                // store current node 
                // in path[]
                localPathList.add(dirEdge.to());
                findPathsRecursively(dirEdge.to(), target, isVisited, localPathList, maxStops);
                 
                // remove current node
                // in path[]
                localPathList.remove(Integer.valueOf(dirEdge.to()));
            }
        }
         
        // Mark the current node
        isVisited[source] = false;
    }

    /**
     * Returns a string representation of this edge-weighted digraph.
     *
     * @return the number of vertices <em>V</em>, followed by the number of edges <em>E</em>,
     *         followed by the <em>V</em> adjacency lists of edges
     */
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(verticiesSize + " " + edgesSize + NEWLINE);
        for (int v = 0; v < verticiesSize; v++) {
            s.append(v + ": ");
            for (DirectedEdge e : adjList[v]) {
                s.append(e + "  ");
            }
            s.append(NEWLINE);
        }
        return s.toString();
    }

    /*
     * Return a integer which represents the distance
     * among all the 'towns' in a direct order.
     */
	public Integer getTotalDistance(List<String> paths) {
		int distance = 0;
		int pathIndex = 1;
		// find the the first vertex
		int start = Dictionary.getInteger(paths.get(0));
		
		//run through every adjacent list,
		//starting at the first path given equivalent
		for (int i = start; i < adjList.length; i++) {
			
			//once we have the list, loop through every edge
			for (DirectedEdge dirEdge : adjList[i]) {
				//chech if the path list is at the end
				if (paths.size() > pathIndex){ 
					//if from and to 'towns' match, get the distance from the edge
					if(dirEdge.to() == Dictionary.getInteger(paths.get(pathIndex)) && 
							Dictionary.getInteger(paths.get(pathIndex-1)) == dirEdge.from()) {
						
						distance += dirEdge.weight();
						pathIndex++;
						break;
					}
				}
				else
				{
					return distance;
				}
			}
		}
		return distance;
	}

}
