package com.avenuecode.model;

import java.util.ArrayList;
import java.util.List;

public class RouteResponsePayload {
	private List<RouteReturn> routes;

	public RouteResponsePayload(){
		this.routes = new ArrayList<>();
	}
	public void addRoute(RouteReturn r){
		routes.add(r);
	}
	public List<RouteReturn> getRoutes() {
		return routes;
	}
	public void setRoutes(List<RouteReturn> routes) {
		this.routes = routes;
	}
	
}
