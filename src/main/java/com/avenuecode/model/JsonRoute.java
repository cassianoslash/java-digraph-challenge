package com.avenuecode.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This class represents a route
 * in json as provided by AC
 * @author Cassiano
 */
@Entity
@Table(name="json_route")
public class JsonRoute {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	private String source;
	private String target;
	private Integer distance;
	
	public JsonRoute() {}
	
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public Integer getDistance() {
		return distance;
	}
	public void setDistance(Integer distance) {
		this.distance = distance;
	}
	
	
}
