package com.avenuecode.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.avenuecode.model.JsonDigraph;
import com.avenuecode.model.JsonRoute;

public class JsonDigraphDAO {
	
	private EntityManager entityManager;

	public JsonDigraphDAO(){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("default");
		this.entityManager = emf.createEntityManager(); 
	}
	
	public boolean save(JsonDigraph jsonDigraph){
		
		entityManager.getTransaction().begin();
		
		for(JsonRoute route : jsonDigraph.getData()){
			entityManager.persist(route);
		}
		
		entityManager.persist(jsonDigraph);
		
		entityManager.getTransaction().commit();
		return false;
		
	}
	
	public JsonDigraph retrieve(int id){
		
		entityManager.getTransaction().begin();
		
		JsonDigraph jsDG = entityManager.find(JsonDigraph.class, id);
				
		entityManager.getTransaction().commit();
		
		return jsDG;
	}
}
