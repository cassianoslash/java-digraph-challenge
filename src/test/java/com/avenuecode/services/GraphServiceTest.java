package com.avenuecode.services;

import static com.jayway.restassured.RestAssured.given;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(GraphService.class)
@EnableWebMvc
public class GraphServiceTest {
     ClassLoader loader = ClassLoader.getSystemClassLoader();

     private String jsonRequestPayload1;
     private String jsonResponsePayloadSavedGraph1;
     private String jsonAvailableRoutes1;
     
	 @Before public void initialize() {
		 try {
			jsonRequestPayload1 = getJsonFromFile("jsonResquestPayload.json");
			
			jsonResponsePayloadSavedGraph1 = getJsonFromFile("jsonResponsePayloadSaveGraph.json");
			
			jsonAvailableRoutes1 = getJsonFromFile("availableRoutes.json");
			
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
		}
	  }

	private String getJsonFromFile(String fileName) throws IOException, URISyntaxException {
		return Files.lines(Paths.get(loader.getResource(fileName).toURI()))
		         .parallel()
		         .collect(Collectors.joining());
	}
     
	@Test
	public void saveGraphAndReturnCreated() throws Exception {
		
		given().content(jsonRequestPayload1)
        .when()
        .post("/graph")
        .then()
        .statusCode(HttpStatus.SC_CREATED);
	}
	
	@Test
	public void saveGraphAndReturnJson() throws Exception {
		
		String json = given().content(jsonRequestPayload1)
        .when()
        .post("/graph").asString();
		
		JSONAssert.assertEquals(jsonResponsePayloadSavedGraph1, json, false);
	}
	
	@Test
	public void retriveGraphOk() throws Exception {
		
		given()
		.pathParam("id", 1)
        .when()
        .get("/graph/{id}")
        .then()
        .statusCode(HttpStatus.SC_OK);
		
		
	}
	
	@Test
	public void retriveGraphJson() throws Exception {
		
		String json = given()
		.pathParam("id", 1)
        .when()
        .get("/graph/{id}").asString();
		
		JSONAssert.assertEquals(jsonResponsePayloadSavedGraph1, json, false);
	}
	
	
	@Test
	public void findAvailableRoutesOk() throws Exception {
		
		given().content(jsonRequestPayload1)
		.pathParam("source", "A")
		.pathParam("target", "C")
        .when()
        .post("/routes/from/{source}/to/{target}?maxStops=")
        .then()
        .statusCode(HttpStatus.SC_OK);
		
	}
	
	@Test
	public void findAvailableRoutesJson() throws Exception {
		
		String json = given().content(jsonRequestPayload1)
		.pathParam("source", "A")
		.pathParam("target", "C")
        .when()
        .post("/routes/from/{source}/to/{target}?maxStops=").asString();
		System.out.println(jsonAvailableRoutes1+" - "+jsonRequestPayload1);
		JSONAssert.assertEquals(jsonAvailableRoutes1, json, false);
	}
}
